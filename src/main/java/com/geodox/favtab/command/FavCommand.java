package com.geodox.favtab.command;

import com.geodox.favtab.tab.data.FavouritesList;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GeoDoX on 2016-02-06.
 */
public class FavCommand extends CommandBase
{
    private static final String ADD = "add";
    private static final String REMOVE = "remove";

    private final List<String> aliases;

    public FavCommand()
    {
        aliases = new ArrayList<String>();
        aliases.add("fav");
        aliases.add("favourite");
    }

    @Override
    public String getCommandName()
    {
        return "fav";
    }

    @Override
    public List<String> getCommandAliases()
    {
        return aliases;
    }

    @Override
    public String getCommandUsage(ICommandSender sender)
    {
        return "fav add|remove <registryName>";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) throws CommandException
    {
        World world = sender.getEntityWorld();

        if(world.isRemote)
        {
            if(args.length != 2)
                sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED.toString() + "Invalid Argument Count"));
            else
            {
                if(args[0].equalsIgnoreCase(ADD))
                {
                    if(FavouritesList.getInstance().addFavourite(args[1]))
                    {
                        ChatComponentText favTab = new ChatComponentText("FavTab:");
                        favTab.getChatStyle().setColor(EnumChatFormatting.YELLOW);

                        ChatComponentText successMessage = new ChatComponentText(" Successfully added '");
                        successMessage.getChatStyle().setColor(EnumChatFormatting.DARK_GREEN);

                        ChatComponentText itemName = new ChatComponentText(args[1]);
                        itemName.getChatStyle().setColor(EnumChatFormatting.LIGHT_PURPLE);

                        ChatComponentText successMessage2 = new ChatComponentText("'!");
                        successMessage2.getChatStyle().setColor(EnumChatFormatting.DARK_GREEN);

                        sender.addChatMessage(favTab.appendSibling(successMessage).appendSibling(itemName).appendSibling(successMessage2));
                    }
                    else
                    {
                        ChatComponentText favTab = new ChatComponentText("FavTab:");
                        favTab.getChatStyle().setColor(EnumChatFormatting.YELLOW);

                        ChatComponentText successMessage = new ChatComponentText(" Failed adding '");
                        successMessage.getChatStyle().setColor(EnumChatFormatting.RED);

                        ChatComponentText itemName = new ChatComponentText(args[1]);
                        itemName.getChatStyle().setColor(EnumChatFormatting.LIGHT_PURPLE);

                        ChatComponentText successMessage2 = new ChatComponentText("'!");
                        successMessage2.getChatStyle().setColor(EnumChatFormatting.RED);

                        sender.addChatMessage(favTab.appendSibling(successMessage).appendSibling(itemName).appendSibling(successMessage2));
                    }
                }

                else if(args[0].equalsIgnoreCase(REMOVE))
                {
                    if(FavouritesList.getInstance().removeFavourite(args[1]))
                    {
                        ChatComponentText favTab = new ChatComponentText("FavTab:");
                        favTab.getChatStyle().setColor(EnumChatFormatting.YELLOW);

                        ChatComponentText successMessage = new ChatComponentText(" Successfully removed '");
                        successMessage.getChatStyle().setColor(EnumChatFormatting.DARK_GREEN);

                        ChatComponentText itemName = new ChatComponentText(args[1]);
                        itemName.getChatStyle().setColor(EnumChatFormatting.LIGHT_PURPLE);

                        ChatComponentText successMessage2 = new ChatComponentText("'!");
                        successMessage2.getChatStyle().setColor(EnumChatFormatting.DARK_GREEN);

                        sender.addChatMessage(favTab.appendSibling(successMessage).appendSibling(itemName).appendSibling(successMessage2));
                    }
                    else
                    {
                        ChatComponentText favTab = new ChatComponentText("FavTab:");
                        favTab.getChatStyle().setColor(EnumChatFormatting.YELLOW);

                        ChatComponentText successMessage = new ChatComponentText(" Failed removing '");
                        successMessage.getChatStyle().setColor(EnumChatFormatting.RED);

                        ChatComponentText itemName = new ChatComponentText(args[1]);
                        itemName.getChatStyle().setColor(EnumChatFormatting.LIGHT_PURPLE);

                        ChatComponentText successMessage2 = new ChatComponentText("'!");
                        successMessage2.getChatStyle().setColor(EnumChatFormatting.RED);

                        sender.addChatMessage(favTab.appendSibling(successMessage).appendSibling(itemName).appendSibling(successMessage2));
                    }
                }
            }
        }
    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender sender)
    {
        return true;
    }
}
