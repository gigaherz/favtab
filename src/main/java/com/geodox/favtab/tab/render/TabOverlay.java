package com.geodox.favtab.tab.render;

import com.geodox.favtab.helper.MouseHelper;
import com.geodox.favtab.resources.ModInfo;
import com.geodox.favtab.tab.FavCreativeTab;
import com.geodox.favtab.tab.data.FavouritesList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.gui.inventory.GuiContainerCreative;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.crash.CrashReport;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ReportedException;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import org.lwjgl.input.Mouse;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by GeoDoX on 2016-02-05.
 */
public class TabOverlay extends Gui
{
    public static final int FAV_CORNER_SIZE = 6;

    private static final ResourceLocation emptyStarLocation = new ResourceLocation(ModInfo.MOD_ID, "textures/favStar.png");
    private static final ResourceLocation filledStarLocation = new ResourceLocation(ModInfo.MOD_ID, "textures/favStarF.png");
    private static final ResourceLocation removeLocation = new ResourceLocation(ModInfo.MOD_ID, "textures/favRemove.png");
    private static final ResourceLocation shortcutTabLocation = new ResourceLocation(ModInfo.MOD_ID, "textures/favTabShortcut.png");
    private static final ResourceLocation shortcutIconTabLocation = new ResourceLocation(ModInfo.MOD_ID, "textures/favTabShortcutIcon.png");

    static Field f_guiLeft;
    static Field f_guiTop;
    static Field f_xSize;
    static Field f_ySize;
    static Field f_tabPage;
    static Method m_setCurrentCreativeTab;

    static
    {
        try
        {
            f_guiLeft = ReflectionHelper.findField(GuiContainer.class, "field_147003_i", "guiLeft");
            f_guiLeft.setAccessible(true);

            f_guiTop = ReflectionHelper.findField(GuiContainer.class, "field_147009_r", "guiTop");
            f_guiTop.setAccessible(true);

            f_xSize = ReflectionHelper.findField(GuiContainer.class, "field_147009_r", "guiTop");
            f_xSize.setAccessible(true);

            f_ySize = ReflectionHelper.findField(GuiContainer.class, "field_147009_r", "guiTop");
            f_ySize.setAccessible(true);

            f_tabPage = ReflectionHelper.findField(GuiContainerCreative.class, "tabPage");
            f_tabPage.setAccessible(true);
        }
        catch (Exception e)
        {
            throw new ReportedException(new CrashReport("Reflecting GuiContainer", e));
        }
    }

    void selectOrRefreshTab(GuiContainerCreative instance)
    {
        if (m_setCurrentCreativeTab == null)
        {
            try
            {
                m_setCurrentCreativeTab = ReflectionHelper.findMethod(GuiContainerCreative.class, instance,
                        new String[]{"func_147050_b", "setCurrentCreativeTab"},
                        CreativeTabs.class);
            }
            catch (Exception e)
            {
                throw new ReportedException(new CrashReport("Reflecting setCurrentCreativeTab", e));
            }
        }

        try
        {
            m_setCurrentCreativeTab.invoke(instance, CreativeTabs.creativeTabArray[FavCreativeTab.instance.getTabIndex()]);
        }
        catch (Exception e)
        {
            throw new ReportedException(new CrashReport("Invoking setCurrentCreativeTab", e));
        }
    }

    @SubscribeEvent
    public void drawScreenPost(GuiScreenEvent.DrawScreenEvent.Post event)
    {
        if(event.gui instanceof GuiContainerCreative)
        {
            GuiContainerCreative containerCreative = (GuiContainerCreative) event.gui;

            if(containerCreative.getSelectedTabIndex() != FavCreativeTab.instance.getTabIndex())
            {
                // 28 is the width/height of the custom tab image ( -1 to overlay)
                int shortcutRenderX = getLeft(containerCreative) - 27;
                int shortcutRenderY = getTop(containerCreative) + 4;

                int shortcutIconRenderX = shortcutRenderX + 7;
                int shortcutIconRenderY = shortcutRenderY + 8;

                Minecraft.getMinecraft().getTextureManager().bindTexture(shortcutTabLocation);
                drawModalRectWithCustomSizedTexture(shortcutRenderX, shortcutRenderY, 0, 0, 28, 32, 28, 32);

                Minecraft.getMinecraft().getTextureManager().bindTexture(shortcutIconTabLocation);
                drawModalRectWithCustomSizedTexture(shortcutIconRenderX, shortcutIconRenderY, 0, 0, 16, 16, 16, 16);
                //drawScaledCustomSizeModalRect(shortcutIconRenderX, shortcutIconRenderY, 0, 0, 11, 11, 15, 15, 11, 11);
            }

            if(getTabPage(containerCreative) == FavCreativeTab.instance.getTabPage())
            {
                boolean isTop = FavCreativeTab.instance.isTabInFirstRow();
                int col = FavCreativeTab.instance.getTabColumn();
                int stackX = getLeft(containerCreative) + 29 * col;
                int stackY = getTop(containerCreative);

                if (isTop)
                {
                    stackY -= 28;
                }
                else
                {
                    stackY += (getHeight(containerCreative) - 4);
                }

                stackX += 6;
                stackY += 8 + (isTop ? 1 : -1);


                Minecraft.getMinecraft().getTextureManager().bindTexture(shortcutIconTabLocation);
                drawModalRectWithCustomSizedTexture(stackX, stackY, 0, 0, 16, 16, 16, 16);
            }

            Slot slotUnderMouse = containerCreative.getSlotUnderMouse();

            if(slotUnderMouse != null && slotUnderMouse.getHasStack())
            {
                int x = getLeft(containerCreative) + slotUnderMouse.xDisplayPosition + (16-FAV_CORNER_SIZE);
                int y = getTop(containerCreative) + slotUnderMouse.yDisplayPosition;

                zLevel += 300;

                GlStateManager.pushMatrix();
                GlStateManager.translate(0, 0, zLevel);


                if(containerCreative.getSelectedTabIndex() == FavCreativeTab.instance.getTabIndex())
                {
                    // If the player is in the favs tab, show remove icon instead
                    Minecraft.getMinecraft().getTextureManager().bindTexture(removeLocation);
                    drawScaledCustomSizeModalRect(x, y, 0f, 0f, 11, 11, FAV_CORNER_SIZE, FAV_CORNER_SIZE, 11, 11);
                }
                else if (FavouritesList.getInstance().isFavourite(slotUnderMouse.getStack()))
                {
                    // If the Item is a favourite, render the opaque Star
                    Minecraft.getMinecraft().getTextureManager().bindTexture(filledStarLocation);
                    drawScaledCustomSizeModalRect(x, y, 0f, 0f, 11, 11, FAV_CORNER_SIZE, FAV_CORNER_SIZE, 11, 11);
                }
                else
                {
                    // If the Item is not a favourite, render the semi-opaque Star
                    Minecraft.getMinecraft().getTextureManager().bindTexture(emptyStarLocation);
                    drawScaledCustomSizeModalRect(x, y, 0f, 0f, 11, 11, FAV_CORNER_SIZE, FAV_CORNER_SIZE, 11, 11);
                }

                GlStateManager.popMatrix();

                zLevel -= 300;
            }
        }
    }

    @SubscribeEvent
    public void mouseClick(GuiScreenEvent.MouseInputEvent.Pre event)
    {
        if(event.gui instanceof GuiContainerCreative)
        {
            // Check if Mouse is over the Favourites Shortcut, if so render hover text

            if(Mouse.getEventButtonState() && Mouse.getEventButton() == 0)
            {
                GuiContainerCreative containerCreative = (GuiContainerCreative) event.gui;

                // Check if Mouse is over the Favourites Shortcut and did Click

                int mouseX = MouseHelper.getScaledMouseX() - getLeft(containerCreative);
                int mouseY = MouseHelper.getScaledMouseY() - getTop(containerCreative);

                if(containerCreative.getSelectedTabIndex() != FavCreativeTab.instance.getTabIndex())
                {
                    // 28 is the width/height of the custom tab image ( -1 to overlay)
                    int shortcutRenderX = - 27;
                    int shortcutRenderY = + 4;

                    if (mouseX >= shortcutRenderX && mouseY >= shortcutRenderY &&
                            mouseX < (shortcutRenderX+28) && mouseY < (shortcutRenderY+28))
                    {
                        setTabPage(containerCreative, FavCreativeTab.instance.getTabPage());
                        selectOrRefreshTab(containerCreative);
                        return;
                    }
                }

                // getSlotUnderMouse() does not appear to work,
                // so we are iterating instead
                Slot slotUnderMouse = null;
                for(Slot slot : containerCreative.inventorySlots.inventorySlots)
                {
                    int favCornerX = slot.xDisplayPosition + (16 - FAV_CORNER_SIZE);
                    int favCornerY = slot.yDisplayPosition;

                    int favCornerXMax = favCornerX + FAV_CORNER_SIZE;
                    int favCornerYMax = favCornerY + FAV_CORNER_SIZE;

                    if((mouseX >= favCornerX && mouseX < favCornerXMax) && (mouseY >= favCornerY && mouseY < favCornerYMax))
                    {
                        slotUnderMouse = slot;
                        break;
                    }
                }

                if (slotUnderMouse != null && slotUnderMouse.getHasStack())
                {
                    ItemStack stack = slotUnderMouse.getStack();
                    if(!FavouritesList.getInstance().isFavourite(stack))
                    {
                        // If the Item is not a favourite, add the favourite
                        FavouritesList.getInstance().addFavourite(stack);
                    }
                    else
                    {
                        // If the Item is a favourite, remove the favourite
                        FavouritesList.getInstance().removeFavourite(stack);
                    }

                    if(containerCreative.getSelectedTabIndex() == FavCreativeTab.instance.getTabIndex())
                        selectOrRefreshTab(containerCreative);

                    event.setCanceled(true);
                }
            }
        }
    }

    private static Integer getTabPage(GuiContainerCreative containerCreative)
    {
        try
        {
            return f_tabPage.getInt(containerCreative);
        } catch (IllegalAccessException e)
        {
            throw new ReportedException(new CrashReport("Reflecting tabPage", e));
        }
    }

    private static void setTabPage(GuiContainerCreative containerCreative, int page)
    {
        try
        {
            f_tabPage.set(containerCreative, page);
        } catch (IllegalAccessException e)
        {
            throw new ReportedException(new CrashReport("Setting tabPage", e));
        }
    }

    private static Integer getLeft(GuiContainerCreative containerCreative)
    {
        try
        {
            return f_guiLeft.getInt(containerCreative);
        } catch (IllegalAccessException e)
        {
            throw new ReportedException(new CrashReport("Reflecting guiLeft", e));
        }
    }

    private static Integer getTop(GuiContainerCreative containerCreative)
    {
        try
        {
            return f_guiTop.getInt(containerCreative);
        } catch (IllegalAccessException e)
        {
            throw new ReportedException(new CrashReport("Reflecting guiTop", e));
        }
    }

    private static Integer getWidth(GuiContainerCreative containerCreative)
    {
        try
        {
            return f_xSize.getInt(containerCreative);
        } catch (IllegalAccessException e)
        {
            throw new ReportedException(new CrashReport("Reflecting guiLeft", e));
        }
    }

    private static Integer getHeight(GuiContainerCreative containerCreative)
    {
        try
        {
            return f_ySize.getInt(containerCreative);
        } catch (IllegalAccessException e)
        {
            throw new ReportedException(new CrashReport("Reflecting guiLeft", e));
        }
    }
}
