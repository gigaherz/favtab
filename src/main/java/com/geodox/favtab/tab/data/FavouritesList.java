package com.geodox.favtab.tab.data;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.*;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by GeoDoX on 2016-01-31.
 */
public class FavouritesList
{
    private static FavouritesList instance;

    private Configuration favouritesFile;
    private Property favouritesProperty;

    private List<ItemStack> favourites;

    public static void init(File favouritesFile)
    {
        if(instance == null)
        {
            instance = new FavouritesList(favouritesFile);
            instance.loadFavourites();
        }
    }

    public FavouritesList(File favsFile)
    {
        favourites = new ArrayList<ItemStack>();
        favouritesFile = new Configuration(favsFile);
        favouritesProperty = favouritesFile.get("favourites", "items", new String[0]);
    }

    public void addFavourite(ItemStack itemStack)
    {
        if(!isFavourite(itemStack))
            favourites.add(itemStack);

        saveFavourites();
    }

    public void removeFavourite(ItemStack itemStack)
    {
        if(isFavourite(itemStack))
        {
            int index = getIndexOfItemStack(itemStack);

            if(index != -1)
                favourites.remove(index);
        }

        saveFavourites();
    }

    public boolean addFavourite(String registeredItemName)
    {
        if(Item.itemRegistry.containsKey(new ResourceLocation(registeredItemName)))
            addFavourite(new ItemStack(Item.itemRegistry.getObject(new ResourceLocation(registeredItemName))));
        else
            return false;
        return true;
    }

    public boolean removeFavourite(String registeredItemName)
    {
        if(Item.itemRegistry.containsKey(new ResourceLocation(registeredItemName)))
            removeFavourite(new ItemStack(Item.itemRegistry.getObject(new ResourceLocation(registeredItemName))));
        else
            return false;
        return true;
    }

    public void saveFavourites()
    {
        String[] favs = new String[favourites.size()];
        for(int i=0;i<favourites.size();i++)
        {
            ItemStack fav = favourites.get(i);

            String favName = Item.itemRegistry.getNameForObject(fav.getItem()).toString();
            int favMeta = fav.getMetadata();

            String favString = favName;

            if(favMeta != 0) favString += "@" + favMeta;

            if(fav.getTagCompound() != null)
            {
                String favTag = fav.getTagCompound().toString();
                favString = favString + favTag;
            }

            favs[i] = favString;
        }

        favouritesProperty.set(favs);
        favouritesFile.save();
    }

    public void loadFavourites()
    {
        favourites.clear();

        String[] favs = favouritesProperty.getStringList();
        for(String favString : favs)
        {
            int split = favString.indexOf('@');
            String favName;
            String favMeta = null;
            String favTag = null;

            if(split >= 0)
            {
                favName = favString.substring(0, split);

                String remaining = favString.substring(split+1);
                int split2 = remaining.indexOf('{');
                if (split2 > 0)
                {
                    favMeta = remaining.substring(0, split2);
                    favTag = remaining.substring(split2);
                }
                else if (split2 == 0)
                {
                    favTag = remaining;
                }
                else
                {
                    favMeta = remaining;
                }
            }
            else
            {
                int split2 = favString.indexOf('{');
                if (split2 >= 0)
                {
                    favName = favString.substring(0, split2);
                    favTag = favString.substring(split2);
                }
                else
                {
                    favName = favString;
                }
            }

            Item item = Item.itemRegistry.getObject(new ResourceLocation(favName));
            if(item == null)
                continue;

            int meta = 0;
            if(favMeta != null && favMeta.length() > 0)
            {
                meta = Integer.parseInt(favMeta);
            }

            NBTTagCompound tag = null;
            if(favTag != null)
            {
                try
                {
                    tag = JsonToNBT.getTagFromJson(favTag);
                }
                catch (NBTException e)
                {
                    // ignore
                }
            }


            ItemStack favStack = new ItemStack(item, 1, meta);
            if(tag != null) favStack.setTagCompound(tag);

            favourites.add(favStack);
        }
    }

    public boolean isFavourite(ItemStack itemStack)
    {
        for (ItemStack i : favourites)
        {
            if(ItemStack.areItemsEqual(i, itemStack) && ItemStack.areItemStackTagsEqual(i, itemStack))
                return true;
        }

        return false;
    }

    private int getIndexOfItemStack(ItemStack itemStack)
    {
        for (ItemStack i : favourites)
        {
            if(ItemStack.areItemsEqual(i, itemStack) && ItemStack.areItemStackTagsEqual(i, itemStack))
                return favourites.indexOf(i);
        }

        return -1;
    }

    public List<ItemStack> getFavourites()
    {
        return favourites;
    }

    public static FavouritesList getInstance()
    {
        return instance;
    }
}
