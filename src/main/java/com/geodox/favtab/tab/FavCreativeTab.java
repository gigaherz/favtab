package com.geodox.favtab.tab;

import com.geodox.favtab.FavTab;
import com.geodox.favtab.tab.data.FavouritesList;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.List;

/**
 * Created by GeoDoX on 2016-01-31.
 */
public class FavCreativeTab extends CreativeTabs
{
    public static final String FAV_TAB_LABEL = "Favourites";

    public static FavCreativeTab instance;

    public FavCreativeTab()
    {
        super(FAV_TAB_LABEL);
        instance = this;
    }

    @Override
    public ItemStack getIconItemStack()
    {
        return null;
    }

    @Override
    public Item getTabIconItem()
    {
        return null;
    }

    @Override
    public void displayAllReleventItems(List<ItemStack> displayedItemStacks)
    {
        for(ItemStack i : FavouritesList.getInstance().getFavourites())
            displayedItemStacks.add(i);

        super.displayAllReleventItems(displayedItemStacks);
    }
}
