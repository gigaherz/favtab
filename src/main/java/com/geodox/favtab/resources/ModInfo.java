package com.geodox.favtab.resources;

/**
 * Created by GeoDoX on 2016-01-31.
 */
public class ModInfo
{
    public static final String MC_VERSION = "1.8.8";

    public static final String MOD_ID = "favtab";
    public static final String MOD_NAME = "FavTab";
    public static final String MOD_VERSION = "1.1.1";
}
