package com.geodox.favtab.helper;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import org.lwjgl.input.Mouse;

/**
 * Created by GeoDoX on 2016-02-05.
 */
public class MouseHelper
{
    public static int getScaledMouseX()
    {
        Minecraft mc = Minecraft.getMinecraft();

        final ScaledResolution scaledresolution = new ScaledResolution(mc);
        int i1 = scaledresolution.getScaledWidth();
        return Mouse.getX() * i1 / mc.displayWidth;
    }

    public static int getScaledMouseY()
    {
        Minecraft mc = Minecraft.getMinecraft();

        final ScaledResolution scaledresolution = new ScaledResolution(mc);
        int j1 = scaledresolution.getScaledHeight();
        return j1 - Mouse.getY() * j1 / mc.displayHeight - 1;
    }
}
